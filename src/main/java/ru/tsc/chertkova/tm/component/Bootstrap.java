package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.ICommandRepository;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.command.project.*;
import ru.tsc.chertkova.tm.command.system.*;
import ru.tsc.chertkova.tm.command.task.*;
import ru.tsc.chertkova.tm.command.user.*;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.chertkova.tm.exception.system.CommandNotSupportedException;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.CommandRepository;
import ru.tsc.chertkova.tm.repository.ProjectRepository;
import ru.tsc.chertkova.tm.repository.TaskRepository;
import ru.tsc.chertkova.tm.repository.UserRepository;
import ru.tsc.chertkova.tm.service.*;
import ru.tsc.chertkova.tm.util.DateUtil;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new ArgListCommand());
        registry(new CmdListCommand());

        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskShowListByProjectIdCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCascadeRemoveCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindToProjectCommand());

        registry(new UserRegistrationCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserShowProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserUnlockCommand());
        registry(new UserLockCommand());
        registry(new UserRemoveCommand());
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(@Nullable final String[] args) {
        processArgument(args[0]);
        initUsers();
        initData();
        initLogger();
        @NotNull String command = "";
        while (!ExitCommand.NAME.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArgument(@Nullable final String argument) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void initData() {
        taskService.add(new Task("task vot", Status.IN_PROGRESS, DateUtil.toDate("04.10.2019")));
        taskService.add(new Task("task kak-to", Status.NOT_STARTED, DateUtil.toDate("04.12.2019")));
        taskService.add(new Task("task tak", Status.COMPLETED, DateUtil.toDate("04.9.2019")));
        projectService.add(new Project("project no", Status.COMPLETED, DateUtil.toDate("04.10.2018")));
        projectService.add(new Project("project nikak", Status.IN_PROGRESS, DateUtil.toDate("04.10.2021")));
        projectService.add(new Project("project inache", Status.NOT_STARTED, DateUtil.toDate("04.10.2020")));
    }

}
