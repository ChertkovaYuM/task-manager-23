package ru.tsc.chertkova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "12345678";

    @NotNull
    Integer ITERATION = 26434;

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        try {
            @NotNull MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@Nullable NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
